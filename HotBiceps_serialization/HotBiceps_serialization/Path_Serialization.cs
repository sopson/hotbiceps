﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Xml;
using System.IO;

namespace FilePath_serialization
{
    public class Path_Serialization
    {
        public void SavePaths(string[] tsciezka)
        {
            XmlTextWriter tw = new XmlTextWriter("PathsSettings.xml", Encoding.Unicode);
            tw.WriteStartDocument();
            tw.WriteStartElement("XMLFILE");

            for (int i = 0; i <= 9; i++)
            {
                tw.WriteStartElement("Path_" + i.ToString());
                tw.WriteString(tsciezka[i]);
                tw.WriteEndElement();
            }

            tw.WriteEndElement();
            tw.WriteEndDocument();
            tw.Close();
        }

        public string LoadPaths(string ChoseNode)
        {
            XmlDocument xdoc = new XmlDocument();
            xdoc.Load("PathsSettings.xml");

            string path = xdoc.SelectSingleNode(ChoseNode).InnerText;

            return path;
        }
    }
}
