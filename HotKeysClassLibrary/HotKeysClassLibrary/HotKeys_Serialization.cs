﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Xml;
using System.IO;
using System.Windows.Input;
using System.ComponentModel;
using System.Data;
using System.Drawing;

using System.Windows.Forms;
using System.Runtime.Serialization;

namespace HotKeysClassLibrary
{
    public class HotKeys_Serialization
    {
        public void SaveHotKeys ( string[] stringKeys )
        {
            StreamWriter file = new StreamWriter("test.txt", false);
            for (int i = 0; i < stringKeys.Length; i++)
            {
                file.WriteLine(stringKeys[i]);
            }
            file.Close();

            /*XmlTextWriter tw = new XmlTextWriter("HotKeysSettings.xml", Encoding.Unicode);
            tw.WriteStartDocument();
            tw.WriteStartElement("XMLFILE");

            for (int i = 0; i <= 9; i++)
            {
                tw.WriteStartElement("HotKey_" + i.ToString());
                tw.WriteString(stringKeys[i]);
                tw.WriteEndElement();
            }

            tw.WriteEndElement();
            tw.WriteEndDocument();
            tw.Close();*/
        }

        public void SaveNamesOfHotKeys(string[] stringNameKeys)
        {
            TypeConverter converter = TypeDescriptor.GetConverter(typeof(Keys));
            XmlTextWriter tw = new XmlTextWriter("HotKeysNamesSettings.xml", Encoding.Unicode);
            tw.WriteStartDocument();
            tw.WriteStartElement("XMLFILE");

            for (int i = 0; i <= 9; i++)
            {
                tw.WriteStartElement("HotKey_" + i.ToString());
                tw.WriteString(stringNameKeys[i]);
                tw.WriteEndElement();
            }

            tw.WriteEndElement();
            tw.WriteEndDocument();
            tw.Close();
        }

        public string LoadNamesOfKeys(string ChoseNode)
        {
            XmlDocument xdoc = new XmlDocument();
            xdoc.Load("HotKeysNamesSettings.xml");

            string key = xdoc.SelectSingleNode(ChoseNode).InnerText;

            return key;
        }

    }

    [Serializable]
    public class KeyCodeHotKey : ISerializable
    {
        private readonly Keys _keyCode;

        public Keys KeyCode
        {
            get { return _keyCode; }
        }


    }
}
