﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using MetroFramework;
using MetroFramework.Properties;
using MetroFramework.Native;
using MetroFramework.Localization;
using MetroFramework.Interfaces;
using MetroFramework.Forms;
using MetroFramework.Fonts;
using MetroFramework.Drawing;
using MetroFramework.Controls;
using MetroFramework.Components;
using MetroFramework.Animation;

using System.Runtime.InteropServices;
using System.Threading;
using System.IO;
using System.Media;

using FilePath_serialization;
using HotKeysClassLibrary;

namespace HotBiceps
{
    public partial class MainForm : MetroForm
    {
        private Path_Serialization sr = new Path_Serialization();
        private HotKeys_Serialization hkcl = new HotKeys_Serialization();

        public MainForm()
        {
            InitializeComponent();

            /* LOADING PREVIOUSLY SAVED HOTKEY SETTINGS */
            if (File.Exists("HotKeysNamesSettings.xml"))
            {
                List<string> keys = new List<string>();

                for ( int i = 0; i <= 9; i++ )
                    keys.Add(hkcl.LoadNamesOfKeys("XMLFILE/HotKey_" + i.ToString()));

                foreach (string str_keys in keys)
                    HotKeyDataGridView.Rows.Add(str_keys);
            }
            
            /* LOADING PREVIOUSLY SAVED SOUNDFILES */
            if ( File.Exists("PathsSettings.xml") )
            {
                List<string> paths = new List<string>();

                for ( int i = 0; i <= 9; i++ )
                    paths.Add( sr.LoadPaths("XMLFILE/Path_"+i.ToString()) );

                foreach ( string str_path in paths )
                    SciezkiDataGridView.Rows.Add(str_path);
            }
        }

        private void NowyMetroButton_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.InitialDirectory = AppDomain.CurrentDomain.BaseDirectory + "audio";

            ofd.Filter = "WAV files (*.wav)|*.wav";
            ofd.DefaultExt = ".wav";

            ofd.CheckPathExists = true;
            ofd.CheckFileExists = true;

            if ( ofd.ShowDialog() == DialogResult.OK )
            {
                string sciezka = ofd.FileName;

                if ( SciezkiDataGridView.Rows.Count <= 9 )
                {
                    SciezkiDataGridView.Rows.Add( sciezka );
                    SciezkiDataGridView.Refresh();
                }
                else
                    MetroMessageBox.Show( this, "You can't add more than 10 sound files.", 100 );
            }
        }

        private void SciezkiDataGridView_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();

            ofd.InitialDirectory = AppDomain.CurrentDomain.BaseDirectory + "audio";
            ofd.Filter = "WAV files (*.wav)|*.wav";
            ofd.DefaultExt = ".wav";
            ofd.CheckPathExists = true;
            ofd.CheckFileExists = true;

            if ( ofd.ShowDialog() == DialogResult.OK )
            {
                string sciezka = ofd.FileName;

                if ( SciezkiDataGridView.Rows.Count <= 10 )
                    SciezkiDataGridView.Rows[e.RowIndex].Cells[0].Value = sciezka;
            }
        }

        private void WlaczMetroButton_Click(object sender, EventArgs e)
        {
            PreparePaths_Save(true);

        }

        private void PomocMetroButton_Click(object sender, EventArgs e)
        {
            MetroMessageBox.Show(
                this, 
                "Click \'New file\', choose the file and hit \'turn on\'! \n" + 
                "Remember about file format, it has to be PCM 16-bit WAV."
                );
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            HotKeyDataGridView.ClearSelection();
        }

        private void SettingsMetroButton_Click(object sender, EventArgs e)
        {
            PreparePaths_Save();

            SettingsForm sf = new SettingsForm();
            this.Hide();
            sf.Show();
        }

        private void MainForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }        

        private void PreparePaths_Save(bool of = false)
        {
            List<string> initsciez = new List<string>();

            for (int i = 0; i < SciezkiDataGridView.Rows.Count; i++)
            {
                if (SciezkiDataGridView.Rows[i].Cells[0].Value == null)
                    initsciez.Add("");
                else
                    initsciez.Add(SciezkiDataGridView.Rows[i].Cells[0].Value.ToString());
            }

            if ((SciezkiDataGridView.Rows.Count - 9) < 0)
            {
                for (int j = 0; j <= (9 - SciezkiDataGridView.Rows.Count); j++)
                    initsciez.Add("");
            }

            string[] allpaths = initsciez.ToArray();
            sr.SavePaths(allpaths);

            if (of == true)
                ShowOF(allpaths);
        }

        private void ShowOF(string[] allpaths)
        {
            OdtwarzaczForm of = new OdtwarzaczForm(allpaths);
            this.Hide();
            of.Show();
        }

        private void MainForm_Shown(object sender, EventArgs e)
        {
            HotKeyDataGridView.DataSource = null;
            HotKeyDataGridView.Rows.Clear();
            HotKeyDataGridView.Refresh();

            if (File.Exists("HotKeysNamesSettings.xml"))
            {
                List<string> keys = new List<string>();

                for (int i = 0; i <= 9; i++)
                    keys.Add(hkcl.LoadNamesOfKeys("XMLFILE/HotKey_" + i.ToString()));

                foreach (string str_keys in keys)
                    HotKeyDataGridView.Rows.Add(str_keys);
            }
        }

    }
}
