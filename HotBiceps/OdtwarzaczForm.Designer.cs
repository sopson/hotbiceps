﻿namespace HotBiceps
{
    partial class OdtwarzaczForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(OdtwarzaczForm));
            this.HotKeyDataGridView = new System.Windows.Forms.DataGridView();
            this.HotkeyDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.WylaczMetroButton = new MetroFramework.Controls.MetroButton();
            this.PlikDataGridView = new System.Windows.Forms.DataGridView();
            this.label1 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.HotKeyDataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PlikDataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // HotKeyDataGridView
            // 
            this.HotKeyDataGridView.AllowUserToAddRows = false;
            this.HotKeyDataGridView.AllowUserToDeleteRows = false;
            this.HotKeyDataGridView.AllowUserToResizeColumns = false;
            this.HotKeyDataGridView.AllowUserToResizeRows = false;
            this.HotKeyDataGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.HotKeyDataGridView.BackgroundColor = System.Drawing.SystemColors.ActiveCaption;
            this.HotKeyDataGridView.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.HotKeyDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.HotKeyDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.HotkeyDataGridViewTextBoxColumn});
            this.HotKeyDataGridView.Enabled = false;
            this.HotKeyDataGridView.Location = new System.Drawing.Point(223, 84);
            this.HotKeyDataGridView.MultiSelect = false;
            this.HotKeyDataGridView.Name = "HotKeyDataGridView";
            this.HotKeyDataGridView.ReadOnly = true;
            this.HotKeyDataGridView.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            this.HotKeyDataGridView.RowHeadersVisible = false;
            this.HotKeyDataGridView.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.HotKeyDataGridView.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.HotKeyDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.HotKeyDataGridView.ShowCellErrors = false;
            this.HotKeyDataGridView.ShowCellToolTips = false;
            this.HotKeyDataGridView.ShowEditingIcon = false;
            this.HotKeyDataGridView.Size = new System.Drawing.Size(83, 244);
            this.HotKeyDataGridView.TabIndex = 6;
            // 
            // HotkeyDataGridViewTextBoxColumn
            // 
            this.HotkeyDataGridViewTextBoxColumn.HeaderText = "Hotkey";
            this.HotkeyDataGridViewTextBoxColumn.Name = "HotkeyDataGridViewTextBoxColumn";
            this.HotkeyDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // WylaczMetroButton
            // 
            this.WylaczMetroButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.WylaczMetroButton.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.WylaczMetroButton.Location = new System.Drawing.Point(20, 368);
            this.WylaczMetroButton.Name = "WylaczMetroButton";
            this.WylaczMetroButton.Size = new System.Drawing.Size(289, 40);
            this.WylaczMetroButton.TabIndex = 17;
            this.WylaczMetroButton.Text = "Turn off";
            this.WylaczMetroButton.UseSelectable = true;
            this.WylaczMetroButton.Click += new System.EventHandler(this.WylaczMetroButton_Click);
            // 
            // PlikDataGridView
            // 
            this.PlikDataGridView.AllowUserToAddRows = false;
            this.PlikDataGridView.AllowUserToDeleteRows = false;
            this.PlikDataGridView.AllowUserToResizeColumns = false;
            this.PlikDataGridView.AllowUserToResizeRows = false;
            this.PlikDataGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.PlikDataGridView.BackgroundColor = System.Drawing.SystemColors.ActiveCaption;
            this.PlikDataGridView.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.PlikDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.PlikDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1});
            this.PlikDataGridView.Enabled = false;
            this.PlikDataGridView.Location = new System.Drawing.Point(23, 84);
            this.PlikDataGridView.MultiSelect = false;
            this.PlikDataGridView.Name = "PlikDataGridView";
            this.PlikDataGridView.ReadOnly = true;
            this.PlikDataGridView.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            this.PlikDataGridView.RowHeadersVisible = false;
            this.PlikDataGridView.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.PlikDataGridView.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.PlikDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.PlikDataGridView.ShowCellErrors = false;
            this.PlikDataGridView.ShowCellToolTips = false;
            this.PlikDataGridView.ShowEditingIcon = false;
            this.PlikDataGridView.Size = new System.Drawing.Size(182, 244);
            this.PlikDataGridView.TabIndex = 18;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.ForeColor = System.Drawing.SystemColors.ButtonShadow;
            this.label1.Location = new System.Drawing.Point(187, 411);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(138, 13);
            this.label1.TabIndex = 19;
            this.label1.Text = "© Michał SopsoN Sobczak";
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.Image = global::HotBiceps.Properties.Resources.hotbiceps2;
            this.pictureBox1.Location = new System.Drawing.Point(-9, 23);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(344, 423);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 20;
            this.pictureBox1.TabStop = false;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.HeaderText = "File";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            // 
            // OdtwarzaczForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(329, 428);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.PlikDataGridView);
            this.Controls.Add(this.WylaczMetroButton);
            this.Controls.Add(this.HotKeyDataGridView);
            this.Controls.Add(this.pictureBox1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "OdtwarzaczForm";
            this.Resizable = false;
            this.Style = MetroFramework.MetroColorStyle.Red;
            this.Text = "Player";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.OdtwarzaczForm_FormClosed);
            this.Shown += new System.EventHandler(this.OdtwarzaczForm_Shown);
            ((System.ComponentModel.ISupportInitialize)(this.HotKeyDataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PlikDataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView HotKeyDataGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn HotkeyDataGridViewTextBoxColumn;
        private MetroFramework.Controls.MetroButton WylaczMetroButton;
        private System.Windows.Forms.DataGridView PlikDataGridView;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
    }
}